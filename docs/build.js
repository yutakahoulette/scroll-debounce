(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var scrollDebounce = require('../index.js')

function toggleInView(selector, cb) {
  const elms = document.querySelectorAll(selector)
  elms.forEach(function(elm, i) {
    const scrollTop = window.document.body.scrollTop + window.innerHeight
    const elmTop = findOffset(elm)
    const inView = scrollTop >= elmTop
    cb(elm, inView, i)
  })
}

function findOffset(elm) {
  if(!elm) return
  if(elm.offsetTop) return elm.offsetTop
  return findOffset(elm.offsetParent)
}

function toggleSectionClasses() {
  return toggleInView('section', function(elm, inView) {
    if(!inView) {
      elm.classList.remove('in-view')
    } else {
      elm.classList.add('in-view')
    }
  })
}

var section = document.querySelector('section')

for(var i = 0; i < 200; i++) {
  section.parentNode.insertBefore(section.cloneNode(true), section.nextSibling)
}

scrollDebounce([toggleSectionClasses], 12)


},{"../index.js":2}],2:[function(require,module,exports){

// debounce can be between 1 - 12, 1 being most frequent and 10 being least
// 3 is the default
module.exports = function scrollDebounce(cbs, debounce) { 
  var i = 0
  window.document.addEventListener('scroll', function() {
    debounce = debounce || 3
    debounce = debounce > 12 ? 12 : debounce
    debounce = debounce < 1 ? 1 : debounce
    i += 1
    if (i % debounce === 0) return
    cbs.forEach(function(cb){cb()})
  })
}

},{}]},{},[1]);
