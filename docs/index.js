var scrollDebounce = require('../index.js')

function toggleInView(selector, cb) {
  const elms = document.querySelectorAll(selector)
  elms.forEach(function(elm, i) {
    const scrollTop = window.document.body.scrollTop + window.innerHeight
    const elmTop = findOffset(elm)
    const inView = scrollTop >= elmTop
    cb(elm, inView, i)
  })
}

function findOffset(elm) {
  if(!elm) return
  if(elm.offsetTop) return elm.offsetTop
  return findOffset(elm.offsetParent)
}

function toggleSectionClasses() {
  return toggleInView('section', function(elm, inView) {
    if(!inView) {
      elm.classList.remove('in-view')
    } else {
      elm.classList.add('in-view')
    }
  })
}

var section = document.querySelector('section')

for(var i = 0; i < 200; i++) {
  section.parentNode.insertBefore(section.cloneNode(true), section.nextSibling)
}

scrollDebounce([toggleSectionClasses], 12)

