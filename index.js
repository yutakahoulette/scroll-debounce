
// debounce can be between 1 - 12, 1 being most frequent and 10 being least
// 3 is the default
module.exports = function scrollDebounce(cbs, debounce) { 
  var i = 0
  window.document.addEventListener('scroll', function() {
    debounce = debounce || 3
    debounce = debounce > 12 ? 12 : debounce
    debounce = debounce < 1 ? 1 : debounce
    i += 1
    if (i % debounce === 0) return
    cbs.forEach(function(cb){cb()})
  })
}
